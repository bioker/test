package com.betpawa.test.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Bounds User and Currency entities together.
 * Unit and fraction values was chosen to be integers for portability
 * between different storage systems
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    private User user;
    private Currency currency;
    private Integer unit;
    private Integer fraction;
}
