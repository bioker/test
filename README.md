# Test quest

## Description

+ Server and Client apps for emulating withdraw-deposit operations
+ Both apps implemented via gRPC
+ Project divided into several modules
    + `model` - base classes that represent application entities
    + `dao` - abstraction and implementation of data access layer
    + `grpc` - gRPC protobuf definition (proto sources and generated classes)
    + `db` - scripts for database creation
    + `server` - gRPC implementation of the server service
    + `client` - gRPC implementation of the client for the service that 
        emulates real application using
        + Internally client choose the random Round from predefined 
            set of Rounds that are stored in resources as XML file.
            So you can add whenever rounds set you want

### Decisions explanation

+ For DAO layer Spring JDBC was chosen because of
    + ORM bounds us to its implementation and if other
        apps want to use the same database it may be difficult to integrate them
        because of complex representation of objects internally in the database.
    + Sure we can use ORM, but it also brings the complexity of the performance improvement
        that pretty important in such type of applications
+ Money API is used but this part is not completed (for now it cannot work with
    fractional parts of the numbers), but it's absolutely clear that in real app
    that deals with money it's essential, so just need more time to implement it
+ For Server app gRPC non-blocking API was used to improve
    throughput of client requests
+ For Client app gRPC blocking API is used but interaction is
    paralleled via simple java concurrency usage ()
    + Several issues occurred with non-blocking API that takes more
        time for investigation
+ For optimization of server side procedures of database may be considered
    + In this case we will have only one request instead of 3 
        (currency check, account getting, account update)
    + But this solution breaks isolation of the data from the logic and
        should be applied only if it's really necessary
    + Also we need to implement procedures for each database we want to use
+ Server side was changed after test launches
    + DAO layer calls sometimes may produce deadlock
        so they were synchronized. In a less powerful machine the deadlock 
        haven't occured sometimes but in more powerful it occurs always,
        as far as I can tell, because of threads rate
    + Connection pool additional configuration was added in 
        attempts to prevent deadlock
    + DAO calls with several sub-calls (Account entity find methods)
        were chaged to fetch all data in one query (get all columns from 
        all necessary tables and create objects inside row mapper)

## Installation

+ Run `./gradlew clean build` for Linux and `gradlew clean build` for Windows
    + At `server` module test stage `12345` port is used for integration test
        + It can be changed [here](server/src/test/resources/applicationContext.xml),
            in `walletServerApp` bean definition port parameter

## Operation

+ Database scripts
    + [Create tables SQL](db/init/create.sql)
    + [Load test data SQL](db/init/data.sql)
+ Server and Client apps packaged as 'fat' jars
    + To launch them just use
        + [Server start script](start_server.sh)
            + Pay attention that you need to have database available 
                and configured (if you don't wanna do it, just use docker:) see below)
        + [Client start script](start_client.sh)
        + For Windows just copy command and launch it manually
+ For docker users
    + All services were configured as images (postgres db, server and client app)
    + Run `sudo docker-compose up`
        + For daemon mode use `-t` option
    + Environment variables for docker-compose are stored in
        [env file](.env)

## Results 

> Just history of launches. Last results on each machine are current metrics

### Intel Core i5 7200U, 6 GB RAM, Ubuntu 18.04

|Launch number|Operations|Milliseconds|Operations per second|Comment|
|-|-|-|-|-|
|1 |3379|33175|101.8538056|Experimental|
|2 |3335|37431|89.09727231|Experimental|
|3 |3354|33487|100.1582704|Experimental|
|4 |3310|35477|93.29988443|Experimental|
|5 |6695|63993|104.6208179|Experimental|
|6 |6631|66304|100.0090492|Experimental|
|7 |6671|69962|95.35176238|Experimental|
|8 |6700|70975|94.39943642|Experimental|
|9 |6723|73933|90.93368320|Experimental|
|10|6603|24607|268.3382777|PreStable|
|11|6722|28051|239.6349506|Stable|

### Intel Core i5 7300UPro, 16 GB RAM, Ubuntu 18.04

|Launch number|Operations|Milliseconds|Operations per second|Comment|
|-|-|-|-|-|
|1|6672|8220 |811,67883211|Experimental|
|2|6747|8005 |842,84821986|Experimental|
|3|6715|8016 |837,69960079|Experimental|
|4|6636|10508|631,51884278|PreStable|
|5|6613|12577|525,80106543|Stable|
