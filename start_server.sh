#!/usr/bin/env bash
TEST_DB_DRIVER=org.postgresql.Driver \
TEST_DB_URL=jdbc:postgresql://postgres/ \
TEST_DB_USER=postgres \
TEST_DB_PASS='' \
TEST_SERVER_PORT=9999 \
java -jar server/build/libs/server-0.1.0.jar
