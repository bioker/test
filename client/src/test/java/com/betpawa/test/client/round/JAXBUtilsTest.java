package com.betpawa.test.client.round;

import com.betpawa.test.client.operation.BalanceBlockedWalletOperation;
import com.betpawa.test.client.operation.DepositBlockedWalletOperation;
import com.betpawa.test.client.operation.WalletOperation;
import com.betpawa.test.client.operation.WithdrawBlockedWalletOperation;
import org.junit.Assert;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

public class JAXBUtilsTest {

    @Test
    public void test() throws JAXBException {

        DepositBlockedWalletOperation walletOperation1 = new DepositBlockedWalletOperation();
        WithdrawBlockedWalletOperation walletOperation2 = new WithdrawBlockedWalletOperation();
        BalanceBlockedWalletOperation walletOperation3 = new BalanceBlockedWalletOperation();

        walletOperation1.setCurrencyIsoCode("USD");
        walletOperation1.setAmount(100);
        walletOperation2.setCurrencyIsoCode("EUR");
        walletOperation2.setAmount(100);

        Round round = new Round();
        round.getOperations().add(walletOperation1);
        round.getOperations().add(walletOperation2);
        round.getOperations().add(walletOperation3);

        RoundMap roundMap = new RoundMap();
        roundMap.getRoundMap().put(1, round);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JAXBUtils.marshall(roundMap, baos);

        String xml = new String(baos.toByteArray(), StandardCharsets.UTF_8);
        System.out.println(xml);

        RoundMap roundMap1 = JAXBUtils.unmarshall(new ByteArrayInputStream(xml.getBytes()));

        Set<String> classNames = new HashSet<>();
        for (WalletOperation walletOperation : roundMap1.getRoundMap().get(1).getOperations()) {
            classNames.add(walletOperation.getClass().getSimpleName());
        }

        Assert.assertEquals(3, classNames.size());
        Assert.assertTrue(classNames.contains(BalanceBlockedWalletOperation.class.getSimpleName()));
        Assert.assertTrue(classNames.contains(WithdrawBlockedWalletOperation.class.getSimpleName()));
        Assert.assertTrue(classNames.contains(DepositBlockedWalletOperation.class.getSimpleName()));
    }

}
