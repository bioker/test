package com.betpawa.test;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Helper class to count the operations throughput
 */
class Counter {
    static final AtomicInteger finishedOperations = new AtomicInteger(0);
    static final AtomicInteger startedOperations = new AtomicInteger(0);
}
