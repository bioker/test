package com.betpawa.test.client.round;

import com.betpawa.test.client.operation.WalletOperation;

import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * List of {@link WalletOperation}s to perform in an emulation
 */
@XmlRootElement
public class Round implements Cloneable {

    private final List<WalletOperation> operations = new ArrayList<>();

    @XmlElementWrapper
    @XmlAnyElement(lax = true)
    public List<WalletOperation> getOperations() {
        return operations;
    }

    @Override
    protected Round clone() {
        Round clone = new Round();
        for (WalletOperation walletOperation : operations) {
            clone.getOperations().add(walletOperation.clone());
        }
        return clone;
    }
}
