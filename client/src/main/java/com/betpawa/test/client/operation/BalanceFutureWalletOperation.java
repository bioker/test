package com.betpawa.test.client.operation;

import com.betpawa.test.All;
import com.betpawa.test.WalletServerGrpc;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import io.grpc.stub.AbstractStub;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * ATTENTION
 * WIP class. Need to apply callback waiting approach for invoker code
 */
@XmlRootElement
@Slf4j
public class BalanceFutureWalletOperation extends WalletOperation {

    private long startTime;

    @Override
    public void execute(AbstractStub stub, Integer user) {
        try {
            startTime = System.currentTimeMillis();
            ListenableFuture<All.BalanceResponse> future =
                    ((WalletServerGrpc.WalletServerFutureStub) stub)
                            .balance(All.BalanceRequest.newBuilder()
                                    .setUserId(user)
                                    .build());
            Futures.addCallback(future, new FutureCallback<All.BalanceResponse>() {
                @Override
                public void onSuccess(@Nullable All.BalanceResponse response) {
                    log.debug("user {} balance request took {}",
                            System.currentTimeMillis() - startTime);
                    if (response != null) {
                        for (All.Money money : response.getMoneyList()) {
                            log.debug("user {} balance of {} is {}, ",
                                    user, money.getIsoCode(), money.getUnit());
                        }
                    } else {
                        log.warn("response is null");
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    log.warn(String.format("balance request failure, took %s",
                            System.currentTimeMillis() - startTime), t);
                }
            });
        } catch (Exception e) {
            log.error("balance for user {} request error", user);
            log.error("exception logging", e);
        }
    }

    @Override
    public StubType getType() {
        return StubType.FUTURE;
    }

    @Override
    public WalletOperation clone() {
        return new BalanceFutureWalletOperation();
    }
}
