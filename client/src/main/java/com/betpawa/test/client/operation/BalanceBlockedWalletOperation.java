package com.betpawa.test.client.operation;

import com.betpawa.test.All;
import com.betpawa.test.WalletServerGrpc;
import io.grpc.stub.AbstractStub;
import lombok.extern.slf4j.Slf4j;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Slf4j
public class BalanceBlockedWalletOperation extends WalletOperation {

    @Override
    public void execute(AbstractStub stub, Integer user) {
        try {
            All.BalanceResponse response = ((WalletServerGrpc.WalletServerBlockingStub) stub)
                    .balance(All.BalanceRequest.newBuilder()
                            .setUserId(user)
                            .build());
            for (All.Money money : response.getMoneyList()) {
                log.debug("user {} balance of {} is {}",
                        user, money.getIsoCode(), money.getUnit());
            }
        } catch (Exception e) {
            log.error("balance for user {} request error", user);
            log.error("exception logging", e);
        }
    }

    @Override
    public StubType getType() {
        return StubType.BLOCK;
    }

    @Override
    public WalletOperation clone() {
        return new BalanceBlockedWalletOperation();
    }
}
