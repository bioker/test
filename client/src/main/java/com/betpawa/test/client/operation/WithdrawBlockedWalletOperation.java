package com.betpawa.test.client.operation;

import com.betpawa.test.All;
import com.betpawa.test.WalletServerGrpc;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.AbstractStub;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

@XmlRootElement
@Slf4j
public class WithdrawBlockedWalletOperation extends WalletOperation {

    @Setter
    private String currencyIsoCode;

    @Setter
    private Integer amount;

    @Override
    public void execute(AbstractStub stub, Integer user) {
        try {
            ((WalletServerGrpc.WalletServerBlockingStub) stub)
                    .withdraw(All.WithdrawRequest.newBuilder()
                            .setUserId(user)
                            .setMoney(All.Money.newBuilder()
                                    .setIsoCode(currencyIsoCode)
                                    .setUnit(amount)
                                    .setFraction(0)
                                    .build())
                            .build());
            log.debug("withdraw {} in {} request for user {} success",
                    amount, currencyIsoCode, user);
        } catch (StatusRuntimeException e) {
            if (Objects.equals(e.getStatus().getDescription(), "insufficient_funds")) {
                log.debug("withdraw {} in {} request for user {} rejected, insufficient funds",
                        amount, currencyIsoCode, user);
            } else {
                log.error("withdraw {} in {} request for user {} error",
                        amount, currencyIsoCode, user);
                log.error("exception logging", e);
            }
        } catch (Exception e) {
            log.error("withdraw {} in {} request for user {} error",
                    amount, currencyIsoCode, user);
            log.error("exception logging", e);
        }
    }

    @Override
    public StubType getType() {
        return StubType.BLOCK;
    }

    @Override
    public WalletOperation clone() {
        WithdrawBlockedWalletOperation clone = new WithdrawBlockedWalletOperation();
        clone.setCurrencyIsoCode(currencyIsoCode);
        clone.setAmount(amount);
        return clone;
    }

    @XmlElement
    public String getCurrencyIsoCode() {
        return currencyIsoCode;
    }

    @XmlElement
    public Integer getAmount() {
        return amount;
    }
}
