package com.betpawa.test.client.operation;

import io.grpc.stub.AbstractStub;

/**
 * Abstraction of client operation
 * Allows programmatically define arbitrary set of
 * client operations to emulate
 * Non thread-safe! Use {@link #clone()} method to
 * create new instance for your needs
 */
public abstract class WalletOperation implements Cloneable {

    public enum StubType {
        BLOCK, ASYNC, FUTURE
    }

    public abstract void execute(AbstractStub stub, Integer user);

    /**
     * What subtype of {@link io.grpc.stub.AbstractStub}
     * will be used for this operation
     * (passed to {@link #execute(AbstractStub, Integer)})
     */
    public abstract StubType getType();

    @Override
    public abstract WalletOperation clone();
}
