package com.betpawa.test.client.operation;

import com.betpawa.test.All;
import com.betpawa.test.WalletServerGrpc;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import io.grpc.stub.AbstractStub;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * ATTENTION
 * WIP class. Need to apply callback waiting approach for invoker code
 */
@XmlRootElement
@Slf4j
public class DepositFutureWalletOperation extends WalletOperation {

    @Setter
    private String currencyIsoCode;

    @Setter
    private Integer amount;

    private long startTime;

    @Override
    public void execute(AbstractStub stub, Integer user) {
        try {

            startTime = System.currentTimeMillis();

            ListenableFuture<All.EmptyResponse> future =
                    ((WalletServerGrpc.WalletServerFutureStub) stub)
                            .deposit(All.DepositRequest.newBuilder()
                                    .setUserId(user)
                                    .setMoney(All.Money.newBuilder()
                                            .setIsoCode(currencyIsoCode)
                                            .setUnit(amount)
                                            .setFraction(0)
                                            .build())
                                    .build());
            Futures.addCallback(future, new FutureCallback<All.EmptyResponse>() {
                @Override
                public void onSuccess(@Nullable All.EmptyResponse result) {
                    log.debug("deposit {} in {} request for user {} success, took {}",
                            amount, currencyIsoCode, user,
                            System.currentTimeMillis() - startTime);
                }

                @Override
                public void onFailure(Throwable t) {
                    log.warn(String.format("deposit %s in %s request for user " +
                                    "%s failure, took %s",
                            amount, currencyIsoCode, user,
                            System.currentTimeMillis() - startTime), t);
                }
            });
        } catch (Exception e) {
            log.error("deposit {} in {} request for user {} error",
                    amount, currencyIsoCode, user);
            log.error("exception logging", e);
        }
    }

    @Override
    public StubType getType() {
        return StubType.FUTURE;
    }

    @Override
    public WalletOperation clone() {
        DepositFutureWalletOperation clone = new DepositFutureWalletOperation();
        clone.setCurrencyIsoCode(currencyIsoCode);
        clone.setAmount(amount);
        return clone;
    }

    @XmlElement
    public String getCurrencyIsoCode() {
        return currencyIsoCode;
    }

    @XmlElement
    public Integer getAmount() {
        return amount;
    }

}
