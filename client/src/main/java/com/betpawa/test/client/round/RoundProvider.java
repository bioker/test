package com.betpawa.test.client.round;

import lombok.extern.slf4j.Slf4j;

import javax.xml.bind.JAXBException;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Encapsulation of logic to get {@link Round} instance
 */
@Slf4j
public class RoundProvider {

    private RoundMap roundMap;

    public RoundProvider(String resource) throws JAXBException {
        roundMap = JAXBUtils.unmarshall(this.getClass().getResourceAsStream(resource));
    }

    public Round getRandomRound() {
        Integer randKey = ThreadLocalRandom.current().nextInt(1,
                roundMap.getRoundMap().keySet().size() + 1);
        log.debug("get round by key {}", randKey);
        return roundMap.getRoundMap().get(randKey).clone();
    }
}
