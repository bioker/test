package com.betpawa.test.client.round;

import com.betpawa.test.client.operation.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Helper class for SerDe of client operations sets
 */
class JAXBUtils {

    static RoundMap unmarshall(InputStream is) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(
                RoundMap.class, Round.class
                , BalanceBlockedWalletOperation.class
                , DepositBlockedWalletOperation.class
                , WithdrawBlockedWalletOperation.class
                , BalanceFutureWalletOperation.class
                , DepositFutureWalletOperation.class
                , WithdrawFutureWalletOperation.class
        );
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        return (RoundMap) unmarshaller.unmarshal(is);
    }

    static void marshall(RoundMap roundMap, OutputStream os) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(
                RoundMap.class, Round.class
                , BalanceBlockedWalletOperation.class
                , DepositBlockedWalletOperation.class
                , WithdrawBlockedWalletOperation.class
                , BalanceFutureWalletOperation.class
                , DepositFutureWalletOperation.class
                , WithdrawFutureWalletOperation.class
        );
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.marshal(roundMap, os);
    }
}
