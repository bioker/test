package com.betpawa.test.client.round;

import lombok.Getter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;
import java.util.Map;

/**
 * Makes possible to choose between {@link Round}s programmatically.
 * In this case it will be random number (used as key)
 */
@XmlRootElement
class RoundMap {

    @Getter
    @XmlElementWrapper
    @XmlElement
    private final Map<Integer, Round> roundMap = new HashMap<>();

}
