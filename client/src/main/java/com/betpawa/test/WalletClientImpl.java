package com.betpawa.test;

import com.betpawa.test.client.operation.WalletOperation;
import com.betpawa.test.client.round.RoundProvider;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Abstraction for clients emulation logic
 */
@Slf4j
public class WalletClientImpl implements AutoCloseable {

    private final ManagedChannel channel;
    private final WalletServerGrpc.WalletServerBlockingStub blockingStub;
    // async stub operations is not implemented yet
    private final WalletServerGrpc.WalletServerStub asyncStub;
    // future stub operations is not completed yet
    private final WalletServerGrpc.WalletServerFutureStub futureStub;
    private final RoundProvider roundProvider;

    private final Integer numberOfUsers;
    private final Integer numberOfThreads;
    private final Integer numberOfRounds;

    private final ExecutorService executorService;


    WalletClientImpl(String host, int port, RoundProvider roundProvider,
                     Integer numberOfUsers, Integer numberOfThreads, Integer numberOfRounds) {

        channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build();

        blockingStub = WalletServerGrpc.newBlockingStub(channel);
        asyncStub = WalletServerGrpc.newStub(channel);
        futureStub = WalletServerGrpc.newFutureStub(channel);

        this.roundProvider = roundProvider;

        executorService = Executors.newFixedThreadPool(numberOfUsers * numberOfThreads * numberOfRounds);

        this.numberOfUsers = numberOfUsers;
        this.numberOfThreads = numberOfThreads;
        this.numberOfRounds = numberOfRounds;

        log.info("client initialized successfully");
    }

    void startAndWait() {
        IntStream.rangeClosed(1, numberOfUsers)
                .boxed()
                .flatMap(userId -> IntStream.rangeClosed(1, numberOfThreads)
                        .boxed()
                        .flatMap(threadNumber -> IntStream.rangeClosed(1, numberOfRounds)
                                .boxed()
                                .flatMap(roundNumber -> roundProvider.getRandomRound().getOperations()
                                        .stream()
                                        .map(walletOperation -> executorService.submit(operationCallable(walletOperation, userId)))
                                )
                        )
                ).collect(Collectors.toList())
                .forEach(voidFuture -> {
                    try {
                        voidFuture.get();
                    } catch (InterruptedException | ExecutionException e) {
                        log.error("future error", e);
                    }
                });
    }

    private Callable<Void> operationCallable(WalletOperation walletOperation, Integer userId) {
        return () -> {
            long operationStartTime = System.currentTimeMillis();
            synchronized (Counter.startedOperations) {
                Counter.startedOperations.incrementAndGet();
            }
            switch (walletOperation.getType()) {
                case BLOCK: {
                    walletOperation.execute(blockingStub, userId);
                }
                break;
                case ASYNC: {
                    walletOperation.execute(asyncStub, userId);
                }
                break;
                case FUTURE: {
                    walletOperation.execute(futureStub, userId);
                }
                break;
            }
            synchronized (Counter.finishedOperations) {
                Counter.finishedOperations.incrementAndGet();
            }
            log.debug("user {} operation took {} millis", userId,
                    System.currentTimeMillis() - operationStartTime);
            return null;
        };
    }

    @Override
    public void close() {
        log.info("shutdown client");
        // check that resource is initialized and close it
        // exception handling is added to be sure that
        // error with one resource doesn't affect process of
        // closing for another one
        if (channel != null) {
            try {
                channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
            } catch (Exception e) {
                log.warn("can't shutdown channel", e);
            }
        }
        if (executorService != null) {
            try {
                executorService.shutdownNow();
            } catch (Exception e) {
                log.warn("can't shutdown task executor service", e);
            }
        }
    }
}
