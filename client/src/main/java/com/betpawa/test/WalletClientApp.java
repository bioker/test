package com.betpawa.test;

import com.betpawa.test.client.round.RoundProvider;
import lombok.extern.slf4j.Slf4j;

/**
 * Entry point for client emulation CLI
 */
@Slf4j
public class WalletClientApp {

    public static void main(String[] args) {

        if (args.length != 5) {
            System.err.println("Invalid parameters. " +
                    "Usage: WalletClientApp server_host server_port " +
                    "number_of_users number_of_threads number_of_rounds");
            System.exit(1);
        }

        final String serverHost = args[0];
        final int serverPort = Integer.valueOf(args[1]);
        final int numberOfUsers = Integer.valueOf(args[2]);
        final int numberOfThreads = Integer.valueOf(args[3]);
        final int numberOfRounds = Integer.valueOf(args[4]);

        try (WalletClientImpl walletClient =
                     new WalletClientImpl(serverHost, serverPort,
                             new RoundProvider("/roundMap_v1.xml"),
                             numberOfUsers, numberOfThreads, numberOfRounds)) {

            long startTime = System.currentTimeMillis();
            walletClient.startAndWait();
            log.info("{} operations in {} millis",
                    Counter.finishedOperations.get(),
                    System.currentTimeMillis() - startTime);
        } catch (Exception e) {
            log.error("Internal Error", e);
        }

    }
}
