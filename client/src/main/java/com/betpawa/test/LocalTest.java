package com.betpawa.test;

import com.betpawa.test.client.round.RoundProvider;
import lombok.extern.slf4j.Slf4j;

/**
 * Helper class for debugging
 */
@Slf4j
public class LocalTest {
    public static void main(String[] args) {
        try (WalletClientImpl walletClient =
                     new WalletClientImpl("localhost", 8080,
                             new RoundProvider("/roundMap_v1.xml"),
                             10, 10, 10)) {
            long startTime = System.currentTimeMillis();
            walletClient.startAndWait();
            log.info("{} operations in {} millis",
                    Counter.finishedOperations.get(),
                    System.currentTimeMillis() - startTime);
        } catch (Exception e) {
            log.error("Internal Error", e);
        }
    }
}
