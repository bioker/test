create table if not exists usr (
    u_id int not null primary key,
    u_name varchar(100) not null
);
create table if not exists currency (
    c_id int not null primary key,
    c_iso_code varchar(3) not null unique
);
create table if not exists account (
    a_user int not null,
    a_currency int not null,
    a_unit int not null,
    a_fraction int not null,
    primary key (a_user, a_currency),
    foreign key (a_user) references usr(u_id),
    foreign key (a_currency) references currency(c_id)
);
