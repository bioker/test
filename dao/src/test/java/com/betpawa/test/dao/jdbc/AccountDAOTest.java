package com.betpawa.test.dao.jdbc;

import com.betpawa.test.dao.AccountDAO;
import com.betpawa.test.dao.CurrencyDAO;
import com.betpawa.test.dao.UserDAO;
import com.betpawa.test.dao.exception.CurrencyNotFound;
import com.betpawa.test.dao.exception.UserNotFound;
import com.betpawa.test.model.Account;
import com.betpawa.test.model.Currency;
import com.betpawa.test.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AccountDAOTest {

    private static UserDAO userDAO;
    private static CurrencyDAO currencyDAO;
    private static AccountDAO accountDAO;

    @BeforeClass
    public static void init() {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");

        userDAO = context.getBean(UserDAO.class);
        currencyDAO = context.getBean(CurrencyDAO.class);
        accountDAO = context.getBean(AccountDAO.class);
    }

    @Before
    public void cleanTables() {
        accountDAO.deleteAll();
        userDAO.deleteAll();
        currencyDAO.deleteAll();
    }

    @Test
    public void createAndFind() {

        Assert.assertEquals(0, accountDAO.findAll().size());

        User user1 = new User(1, "User 1");
        Currency currency1 = new Currency(1, "USD");
        userDAO.create(user1);
        currencyDAO.create(currency1);
        accountDAO.create(new Account(user1, currency1, 0, 0));

        Assert.assertEquals(0, accountDAO.find(user1, currency1).getUnit().intValue());
        Assert.assertEquals(0, accountDAO.find(user1, currency1).getFraction().intValue());
    }

    @Test
    public void findAll() {

        createTestUser(1, "User 1");
        createTestCurrency(1, "USD");
        createTestCurrency(2, "EUR");
        createTestCurrency(3, "GBP");

        createTestAccount(1, 1);
        createTestAccount(1, 2);
        createTestAccount(1, 3);

        Assert.assertEquals(3, accountDAO.findAll().size());
    }

    @Test
    public void deleteAll() {

        Assert.assertEquals(0, accountDAO.findAll().size());

        createTestUser(1, "User 1");
        createTestCurrency(1, "USD");
        createTestCurrency(2, "EUR");
        createTestCurrency(3, "GBP");

        createTestAccount(1, 1);
        createTestAccount(1, 2);
        createTestAccount(1, 3);

        Assert.assertEquals(3, accountDAO.findAll().size());

        accountDAO.deleteAll();

        Assert.assertEquals(0, accountDAO.findAll().size());
    }

    @Test
    public void update() {

        createTestUser(1, "User 1");
        createTestCurrency(1, "USD");
        createTestCurrency(2, "EUR");
        createTestCurrency(3, "GBP");

        createTestAccount(1, 1, 1, 3);

        Assert.assertEquals(1, accountDAO.find(new User(1, ""),
                new Currency(1, "")).getUnit().intValue());
        Assert.assertEquals(3, accountDAO.find(new User(1, ""),
                new Currency(1, "")).getFraction().intValue());

        accountDAO.update(new Account(
                new User(1, ""),
                new Currency(1, ""),
                3, 1
        ));

        Assert.assertEquals(3, accountDAO.find(new User(1, ""),
                new Currency(1, "")).getUnit().intValue());
        Assert.assertEquals(1, accountDAO.find(new User(1, ""),
                new Currency(1, "")).getFraction().intValue());
    }

    @Test
    public void delete() {

        Assert.assertEquals(0, accountDAO.findAll().size());

        createTestUser(1, "User 1");
        createTestCurrency(1, "USD");
        createTestCurrency(2, "EUR");
        createTestCurrency(3, "GBP");

        createTestAccount(1, 1, 1, 3);

        Assert.assertEquals(1, accountDAO.findAll().size());

        accountDAO.delete(
                new Account(
                        new User(1, ""),
                        new Currency(1, ""),
                        0, 0
                )
        );

        Assert.assertEquals(0, accountDAO.findAll().size());
    }

    @Test
    public void findByUser() {

        Assert.assertEquals(0, accountDAO.findAll().size());

        createTestUser(1, "User 1");
        createTestCurrency(1, "USD");
        createTestCurrency(2, "EUR");
        createTestCurrency(3, "GBP");

        createTestAccount(1, 1);
        createTestAccount(1, 2);
        createTestAccount(1, 3);

        Assert.assertEquals(3, accountDAO.find(new User(1, "")).size());
    }

    @Test(expected = UserNotFound.class)
    public void userNotFound() {

        createTestCurrency(1, "USD");
        createTestAccount(1, 1);

    }

    @Test(expected = CurrencyNotFound.class)
    public void currencyNotFound() {

        createTestUser(1, "");
        createTestAccount(1, 1);

    }

    private void createTestUser(Integer id, String name) {
        userDAO.create(new User(id, name));
    }

    private void createTestCurrency(Integer id, String isoCode) {
        currencyDAO.create(new Currency(id, isoCode));
    }

    private void createTestAccount(Integer userId, Integer currencyId) {
        createTestAccount(userId, currencyId, 0, 0);
    }

    private void createTestAccount(Integer userId, Integer currencyId, Integer unit, Integer fraction) {
        accountDAO.create(new Account(
                new User(userId, ""),
                new Currency(currencyId, ""), unit, fraction));
    }

}
