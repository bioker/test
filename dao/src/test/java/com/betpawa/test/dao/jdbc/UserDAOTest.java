package com.betpawa.test.dao.jdbc;

import com.betpawa.test.dao.UserDAO;
import com.betpawa.test.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UserDAOTest {

    private static UserDAO userDAO;

    @BeforeClass
    public static void init() {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");
        userDAO = context.getBean(UserDAO.class);
    }

    @Before
    public void cleanTable() {
        userDAO.deleteAll();
    }

    @Test
    public void createAndFind() {

        User user = new User();
        user.setId(1);
        user.setName("User 1");

        userDAO.create(user);

        Assert.assertEquals("User 1", userDAO.find(1).getName());
    }

    @Test
    public void findAll() {

        User user1 = new User();
        user1.setId(1);
        user1.setName("User 1");

        User user2 = new User();
        user2.setId(2);
        user2.setName("User 2");

        userDAO.create(user1);
        userDAO.create(user2);

        Assert.assertEquals(2, userDAO.findAll().size());
    }

    @Test
    public void deleteAll() {

        Assert.assertEquals(0, userDAO.findAll().size());

        User user = new User();
        user.setId(3);
        user.setName("User 3");

        userDAO.create(user);

        Assert.assertEquals(1, userDAO.findAll().size());

        User user2 = new User();
        user2.setId(2);
        user2.setName("User 2");

        userDAO.create(user2);

        Assert.assertEquals(2, userDAO.findAll().size());

        userDAO.deleteAll();

        Assert.assertEquals(0, userDAO.findAll().size());
    }

    @Test
    public void update() {

        User user = new User();
        user.setId(1);
        user.setName("User 1");

        userDAO.create(user);

        Assert.assertEquals("User 1", userDAO.find(1).getName());

        user.setName("Updated User 1");

        userDAO.update(user);

        Assert.assertEquals("Updated User 1", userDAO.find(1).getName());
    }

    @Test
    public void delete() {

        User user = new User();
        user.setId(1);
        user.setName("User 1");

        userDAO.create(user);

        Assert.assertEquals(1, userDAO.findAll().size());

        userDAO.delete(user);

        Assert.assertEquals(0, userDAO.findAll().size());
    }

    @Test
    public void exists() {

        User user = new User();
        user.setId(1);
        user.setName("User 1");

        userDAO.create(user);

        Assert.assertEquals("User 1", userDAO.find(1).getName());

        Assert.assertTrue(userDAO.exists(user));
    }

}
