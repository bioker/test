package com.betpawa.test.dao.jdbc;

import com.betpawa.test.dao.CurrencyDAO;
import com.betpawa.test.model.Currency;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DuplicateKeyException;

public class CurrencyDAOTest {

    private static CurrencyDAO currencyDAO;

    @BeforeClass
    public static void init() {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");
        currencyDAO = context.getBean(CurrencyDAO.class);
    }

    @Before
    public void cleanTable() {
        currencyDAO.deleteAll();
    }

    @Test
    public void createAndFind() {

        Assert.assertEquals(0, currencyDAO.findAll().size());

        Currency currency = new Currency();
        currency.setId(1);
        currency.setIsoCode("USD");

        currencyDAO.create(currency);

        Assert.assertEquals("USD", currencyDAO.find(currency.getId()).getIsoCode());
    }

    @Test
    public void findAll() {

        Currency currency1 = new Currency();
        currency1.setId(1);
        currency1.setIsoCode("USD");

        Currency currency2 = new Currency();
        currency2.setId(2);
        currency2.setIsoCode("EUR");

        currencyDAO.create(currency1);
        currencyDAO.create(currency2);

        Assert.assertEquals(2, currencyDAO.findAll().size());
    }

    @Test
    public void deleteAll() {

        Currency currency1 = new Currency();
        currency1.setId(1);
        currency1.setIsoCode("USD");

        currencyDAO.create(currency1);

        Assert.assertEquals(1, currencyDAO.findAll().size());

        currencyDAO.deleteAll();

        Assert.assertEquals(0, currencyDAO.findAll().size());
    }

    @Test
    public void update() {

        Currency currency1 = new Currency();
        currency1.setId(1);
        currency1.setIsoCode("USD");

        currencyDAO.create(currency1);

        Assert.assertEquals("USD", currencyDAO.find(currency1.getId()).getIsoCode());

        currency1.setIsoCode("EUR");

        currencyDAO.update(currency1);

        Assert.assertEquals("EUR", currencyDAO.find(currency1.getId()).getIsoCode());
    }

    @Test
    public void delete() {

        Currency currency1 = new Currency();
        currency1.setId(1);
        currency1.setIsoCode("USD");

        Currency currency2 = new Currency();
        currency2.setId(2);
        currency2.setIsoCode("EUR");

        currencyDAO.create(currency1);
        currencyDAO.create(currency2);

        Assert.assertEquals(2, currencyDAO.findAll().size());

        currencyDAO.delete(currency1);

        Assert.assertEquals(1, currencyDAO.findAll().size());
    }

    @Test
    public void exists() {

        Currency currency1 = new Currency();
        currency1.setId(1);
        currency1.setIsoCode("USD");

        currencyDAO.create(currency1);

        Assert.assertEquals("USD", currencyDAO.find(1).getIsoCode());

        Assert.assertTrue(currencyDAO.exists(currency1));
    }

    @Test
    public void findByIsoCode() {

        Currency currency1 = new Currency();
        currency1.setId(1);
        currency1.setIsoCode("USD");

        currencyDAO.create(currency1);

        Assert.assertEquals(1, currencyDAO.find("USD").getId().intValue());
    }

    @Test(expected = DuplicateKeyException.class)
    public void uniqueConstraint() {

        Currency currency1 = new Currency();
        currency1.setId(1);
        currency1.setIsoCode("USD");

        Currency currency2 = new Currency();
        currency2.setId(2);
        currency2.setIsoCode("USD");

        currencyDAO.create(currency1);
        currencyDAO.create(currency2);
    }

}
