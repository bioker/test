package com.betpawa.test.dao;

import com.betpawa.test.model.Account;
import com.betpawa.test.model.Currency;
import com.betpawa.test.model.User;

import java.util.List;

public interface AccountDAO {

    Account find(User user, Currency currency);

    List<Account> find(User user);

    int create(Account account);

    List<Account> findAll();

    int delete(Account account);

    int deleteAll();

    int update(Account account);
}
