package com.betpawa.test.dao;

import com.betpawa.test.model.User;

import java.util.List;

public interface UserDAO {

    User find(Integer id);

    List<User> findAll();

    int create(User user);

    int update(User user);

    int delete(User user);

    int deleteAll();

    boolean exists(User user);

}
