package com.betpawa.test.dao.exception;

import com.betpawa.test.model.User;
import lombok.Getter;

public class UserNotFound extends TestDAOException {

    @Getter
    private User user;

    public UserNotFound(User user) {
        super("User not found by id: " + user.getId());
        this.user = user;
    }

}
