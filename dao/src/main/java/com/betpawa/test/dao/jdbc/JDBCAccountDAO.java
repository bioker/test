package com.betpawa.test.dao.jdbc;

import com.betpawa.test.dao.*;
import com.betpawa.test.dao.exception.AccountNotFound;
import com.betpawa.test.dao.exception.CurrencyNotFound;
import com.betpawa.test.dao.exception.UserNotFound;
import com.betpawa.test.model.Account;
import com.betpawa.test.model.Currency;
import com.betpawa.test.model.User;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;

@Slf4j
public class JDBCAccountDAO implements AccountDAO, SQLConstants {

    private static final String SELECT_ALL_SQL =
            SQLUtils.selectAllSQL(ACCOUNT_TABLE);

    private static final String UPDATE_SQL =
            String.format("update %s set %s = ?, %s = ? where %s = ? and %s = ?",
                    ACCOUNT_TABLE, ACCOUNT_UNIT_COLUMN, ACCOUNT_FRACTION_COLUMN,
                    ACCOUNT_USER_COLUMN, ACCOUNT_CURRENCY_COLUMN);

    private static final String DELETE_SQL =
            String.format("delete from %s where %s = ? and %s = ?",
                    ACCOUNT_TABLE, ACCOUNT_USER_COLUMN, ACCOUNT_CURRENCY_COLUMN);

    private static final RowMapper<Account> ROW_MAPPER = (resultSet, i) -> {
        Account account = new Account();
        account.setUser(new User(
                resultSet.getInt(1),
                resultSet.getString(6)
        ));
        account.setCurrency(new Currency(
                resultSet.getInt(2),
                resultSet.getString(8)
        ));
        account.setUnit(resultSet.getInt(3));
        account.setFraction(resultSet.getInt(4));
        return account;
    };

    @Setter
    @Getter
    private JdbcTemplate jdbcTemplate;
    @Setter
    @Getter
    private UserDAO userDAO;
    @Setter
    @Getter
    private CurrencyDAO currencyDAO;

    @Override
    public Account find(User user, Currency currency) {
        log.debug("getting account by user {} and currency {}", user.getId(), currency.getId());
        try {
            return jdbcTemplate.queryForObject(SQLUtils.findAccountByUserAndCurrencySQL(),
                    ROW_MAPPER, user.getId(), currency.getId());
        } catch (EmptyResultDataAccessException e) {
            throw new AccountNotFound(new Account(user, currency, -1, -1));
        }
    }

    @Override
    public List<Account> find(User user) {
        log.debug("getting accounts for user", user.getId());
        return jdbcTemplate.query(SQLUtils.findAccountsByUserSQL(), ROW_MAPPER, user.getId());
    }

    @Override
    public int create(Account account) {
        log.debug("creating account with user {} and currency {}",
                account.getUser().getId(),
                account.getCurrency().getId());
        if (!userDAO.exists(account.getUser())) {
            throw new UserNotFound(account.getUser());
        }
        if (!currencyDAO.exists(account.getCurrency())) {
            throw new CurrencyNotFound(account.getCurrency());
        }
        return jdbcTemplate.update(
                String.format("insert into %s values(?, ?, ?, ?)", ACCOUNT_TABLE),
                account.getUser().getId(), account.getCurrency().getId(),
                account.getUnit(), account.getFraction()
        );
    }

    @Override
    public List<Account> findAll() {
        log.debug("getting all accounts");
        return jdbcTemplate.query(SELECT_ALL_SQL, (resultSet, i) -> {
            Account account = new Account();
            account.setUser(userDAO.find(resultSet.getInt(1)));
            account.setCurrency(currencyDAO.find(resultSet.getInt(2)));
            account.setUnit(resultSet.getInt(3));
            account.setFraction(resultSet.getInt(4));
            return account;
        });
    }

    @Override
    public int delete(Account account) {
        log.debug("deleting account for user {} and currency {}",
                account.getUser().getId(),
                account.getCurrency().getId());
        return jdbcTemplate.update(DELETE_SQL,
                account.getUser().getId(), account.getCurrency().getId());
    }

    @Override
    public int deleteAll() {
        log.debug("deleting all accounts");
        return jdbcTemplate.update(SQLUtils.deleteAllSQL(ACCOUNT_TABLE));
    }

    @Override
    public int update(Account account) {
        log.debug("update account for user {} and currency {}",
                account.getUser().getId(),
                account.getCurrency().getId());
        return jdbcTemplate.update(UPDATE_SQL,
                account.getUnit(), account.getFraction(),
                account.getUser().getId(), account.getCurrency().getId()
        );
    }

}
