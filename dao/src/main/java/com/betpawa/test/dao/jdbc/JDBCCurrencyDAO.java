package com.betpawa.test.dao.jdbc;

import com.betpawa.test.dao.CurrencyDAO;
import com.betpawa.test.dao.SQLConstants;
import com.betpawa.test.dao.SQLUtils;
import com.betpawa.test.dao.exception.CurrencyNotFound;
import com.betpawa.test.model.Currency;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;

@Slf4j
public class JDBCCurrencyDAO implements CurrencyDAO, SQLConstants {

    private static final String SELECT_ONE_SQL =
            SQLUtils.selectOneSQL(CURRENCY_TABLE, CURRENCY_ID_COLUMN);
    private static final String SELECT_ONE_SQL_BY_CODE =
            SQLUtils.selectOneSQL(CURRENCY_TABLE, CURRENCY_ISOCODE_COLUMN);
    private static final String SELECT_ALL_SQL =
            SQLUtils.selectAllSQL(CURRENCY_TABLE);
    private static final String SELECT_COUNT =
            SQLUtils.selectCount(CURRENCY_TABLE, CURRENCY_ID_COLUMN);

    private static final RowMapper<Currency> ROW_MAPPER = (resultSet, num) -> {
        Currency currency = new Currency();
        currency.setId(resultSet.getInt(1));
        currency.setIsoCode(resultSet.getString(2));
        return currency;
    };

    @Setter
    @Getter
    private JdbcTemplate jdbcTemplate;

    @Override
    public Currency find(Integer id) {
        log.debug("getting currency by id {}", id);
        try {
            return jdbcTemplate.queryForObject(SELECT_ONE_SQL, ROW_MAPPER, id);
        } catch (EmptyResultDataAccessException e) {
            throw new CurrencyNotFound(new Currency(id, ""));
        }
    }

    @Override
    public Currency find(String isoCode) {
        log.debug("getting currency by iso code {}", isoCode);
        try {
            return jdbcTemplate.queryForObject(SELECT_ONE_SQL_BY_CODE, ROW_MAPPER, isoCode);
        } catch (EmptyResultDataAccessException e) {
            throw new CurrencyNotFound(new Currency(-1, isoCode));
        }
    }

    @Override
    public List<Currency> findAll() {
        log.debug("getting all currencies");
        return jdbcTemplate.query(SELECT_ALL_SQL, ROW_MAPPER);
    }

    @Override
    public int create(Currency currency) {
        log.debug("creating currency with id {} and iso code {}",
                currency.getId(), currency.getIsoCode());
        return jdbcTemplate.update(
                String.format("insert into %s values (?, ?)", CURRENCY_TABLE),
                currency.getId(), currency.getIsoCode());
    }

    @Override
    public int update(Currency currency) {
        log.debug("updating currency with id {} and iso code {}",
                currency.getId(), currency.getIsoCode());
        return jdbcTemplate.update(String.format("update %s set %s = ? where %s = ?",
                CURRENCY_TABLE, CURRENCY_ISOCODE_COLUMN, CURRENCY_ID_COLUMN),
                currency.getIsoCode(), currency.getId());
    }

    @Override
    public int delete(Currency currency) {
        log.debug("deleting currency with id {}", currency.getId());
        return jdbcTemplate.update(String.format("delete from %s where %s = ?",
                CURRENCY_TABLE, CURRENCY_ID_COLUMN), currency.getId());
    }

    @Override
    public int deleteAll() {
        log.debug("deleting all currencies");
        return jdbcTemplate.update(SQLUtils.deleteAllSQL(CURRENCY_TABLE));
    }

    @Override
    public boolean exists(Currency currency) {
        log.debug("checking currency with id {}", currency.getId());
        Integer result = jdbcTemplate.queryForObject(SELECT_COUNT, Integer.class, currency.getId());
        return result != null && result > 0;
    }

}
