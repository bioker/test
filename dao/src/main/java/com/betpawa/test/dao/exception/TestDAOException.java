package com.betpawa.test.dao.exception;

/**
 * Generic exception for working with project DAO implementations
 */
public class TestDAOException extends RuntimeException {

    public TestDAOException(String message) {
        super(message);
    }
}
