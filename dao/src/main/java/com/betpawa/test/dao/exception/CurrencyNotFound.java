package com.betpawa.test.dao.exception;

import com.betpawa.test.model.Currency;
import lombok.Getter;

public class CurrencyNotFound extends TestDAOException {

    @Getter
    private Currency currency;

    public CurrencyNotFound(Currency currency) {
        super("Currency not found by id: " + currency.getId());
        this.currency = currency;
    }

}
