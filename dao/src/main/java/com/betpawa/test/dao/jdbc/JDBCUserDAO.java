package com.betpawa.test.dao.jdbc;

import com.betpawa.test.dao.SQLConstants;
import com.betpawa.test.dao.SQLUtils;
import com.betpawa.test.dao.UserDAO;
import com.betpawa.test.dao.exception.UserNotFound;
import com.betpawa.test.model.User;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.util.List;

@Slf4j
public class JDBCUserDAO implements UserDAO, SQLConstants {

    private static final String SELECT_ONE_SQL =
            SQLUtils.selectOneSQL(USER_TABLE, USER_ID_COLUMN);
    private static final String DELETE_ONE_SQL =
            SQLUtils.deleteOneSQL(USER_TABLE, USER_ID_COLUMN);
    private static final String SELECT_ALL_SQL =
            SQLUtils.selectAllSQL(USER_TABLE);
    private static final String SELECT_COUNT =
            SQLUtils.selectCount(USER_TABLE, USER_ID_COLUMN);

    private static final RowMapper<User> ROW_MAPPER = (resultSet, num) -> {
        User user = new User();
        user.setId(resultSet.getInt(1));
        user.setName(resultSet.getString(2));
        return user;
    };

    @Setter
    @Getter
    private JdbcTemplate jdbcTemplate;

    @Override
    public User find(Integer id) {
        log.debug("getting user by id {}", id);
        try {
            return jdbcTemplate.queryForObject(SELECT_ONE_SQL, ROW_MAPPER, id);
        } catch (EmptyResultDataAccessException e) {
            throw new UserNotFound(new User(id, ""));
        }
    }

    @Override
    public List<User> findAll() {
        log.debug("getting all users");
        return jdbcTemplate.query(SELECT_ALL_SQL, ROW_MAPPER);
    }

    @Override
    public int create(User user) {
        log.debug("creating user with id {} and name {}",
                user.getId(),
                user.getName());
        return jdbcTemplate.update(
                String.format("insert into %s values (?, ?)", USER_TABLE),
                user.getId(), user.getName());
    }

    @Override
    public int update(User user) {
        log.debug("updating user with id {} and name {}",
                user.getId(),
                user.getName());
        return jdbcTemplate.update(
                String.format("update %s set %s = ? where %s = ?",
                        USER_TABLE, USER_NAME_COLUMN, USER_ID_COLUMN),
                user.getName(), user.getId());
    }

    @Override
    public int delete(User user) {
        log.debug("deleting user with id {}", user.getId());
        return jdbcTemplate.update(DELETE_ONE_SQL, user.getId());
    }

    @Override
    public int deleteAll() {
        log.debug("deleting all users");
        return jdbcTemplate.update(SQLUtils.deleteAllSQL(USER_TABLE));
    }

    @Override
    public boolean exists(User user) {
        log.debug("checking user with id {}", user.getId());
        Integer result = jdbcTemplate.queryForObject(SELECT_COUNT, Integer.class, user.getId());
        return result != null && result > 0;
    }

}
