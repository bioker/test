package com.betpawa.test.dao;

/**
 * Helper for making SQL queries
 */
public class SQLUtils implements SQLConstants {

    public static String selectOneSQL(String tableName, String idColumn) {
        return String.format("select * from %s where %s = ?",
                tableName, idColumn);
    }

    public static String deleteOneSQL(String tableName, String idColumn) {
        return String.format("delete from %s where %s = ?",
                tableName, idColumn);
    }

    public static String selectAllSQL(String tableName) {
        return String.format("select * from %s", tableName);
    }

    public static String deleteAllSQL(String tableName) {
        return String.format("delete from %s",
                tableName);
    }

    public static String selectCount(String tableName, String idColumn) {
        return String.format("select count(*) from %s where %s = ?",
                tableName, idColumn);
    }

    // used for improve performance by performing one db call instead of three
    public static String findAccountByUserAndCurrencySQL() {
        return String.format("select %s.*, %s.*, %s.* from %s, %s, %s " +
                        "where %s.%s = %s.%s and %s.%s = %s.%s and %s.%s = ? and %s.%s = ?",
                // select
                ACCOUNT_TABLE,
                USER_TABLE,
                CURRENCY_TABLE,
                // from
                ACCOUNT_TABLE,
                USER_TABLE,
                CURRENCY_TABLE,
                // where
                ACCOUNT_TABLE,
                ACCOUNT_USER_COLUMN,
                // =
                USER_TABLE,
                USER_ID_COLUMN,
                // and
                ACCOUNT_TABLE,
                ACCOUNT_CURRENCY_COLUMN,
                // =
                CURRENCY_TABLE,
                CURRENCY_ID_COLUMN,
                // and
                ACCOUNT_TABLE,
                ACCOUNT_USER_COLUMN,
                // = ? and
                ACCOUNT_TABLE,
                ACCOUNT_CURRENCY_COLUMN
                // = ?
        );
    }

    // used for improve performance by performing one db call instead of three
    public static String findAccountsByUserSQL() {
        return String.format("select %s.*, %s.*, %s.* from %s, %s, %s " +
                        "where %s.%s = %s.%s and %s.%s = %s.%s and %s.%s = ?",
                // select
                ACCOUNT_TABLE,
                USER_TABLE,
                CURRENCY_TABLE,
                // from
                ACCOUNT_TABLE,
                USER_TABLE,
                CURRENCY_TABLE,
                // where
                ACCOUNT_TABLE,
                ACCOUNT_USER_COLUMN,
                // =
                USER_TABLE,
                USER_ID_COLUMN,
                // and
                ACCOUNT_TABLE,
                ACCOUNT_CURRENCY_COLUMN,
                // =
                CURRENCY_TABLE,
                CURRENCY_ID_COLUMN,
                // and
                ACCOUNT_TABLE,
                ACCOUNT_USER_COLUMN
        );
    }
}
