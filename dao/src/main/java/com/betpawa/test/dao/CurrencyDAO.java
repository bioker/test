package com.betpawa.test.dao;

import com.betpawa.test.model.Currency;

import java.util.List;

/**
 * Currency entity is stored in an external storage
 * for the flexible control of supported currencies
 */
public interface CurrencyDAO {

    Currency find(Integer id);

    Currency find(String isoCode);

    List<Currency> findAll();

    int create(Currency currency);

    int update(Currency currency);

    int delete(Currency currency);

    int deleteAll();

    boolean exists(Currency currency);

}
