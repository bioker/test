package com.betpawa.test.dao.exception;

import com.betpawa.test.model.Account;
import lombok.Getter;

public class AccountNotFound extends TestDAOException {

    @Getter
    private Account account;

    public AccountNotFound(Account account) {
        super("Account not found for user and currency: " +
                account.getUser().getId() + " " + account.getCurrency().getId());
        this.account = account;
    }

}
