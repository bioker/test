package com.betpawa.test.dao;

public interface SQLConstants {

    String USER_TABLE = "usr";
    String USER_ID_COLUMN = "u_id";
    String USER_NAME_COLUMN = "u_name";

    String CURRENCY_TABLE = "currency";
    String CURRENCY_ID_COLUMN = "c_id";
    String CURRENCY_ISOCODE_COLUMN = "c_iso_code";

    String ACCOUNT_TABLE = "account";
    String ACCOUNT_USER_COLUMN = "a_user";
    String ACCOUNT_CURRENCY_COLUMN = "a_currency";
    String ACCOUNT_UNIT_COLUMN = "a_unit";
    String ACCOUNT_FRACTION_COLUMN = "a_fraction";

}
