package com.betpawa.test;

import com.betpawa.test.dao.AccountDAO;
import com.betpawa.test.dao.CurrencyDAO;
import com.betpawa.test.dao.UserDAO;
import com.betpawa.test.dao.exception.AccountNotFound;
import com.betpawa.test.dao.exception.CurrencyNotFound;
import com.betpawa.test.dao.exception.UserNotFound;
import com.betpawa.test.exception.DataNotUpdated;
import com.betpawa.test.exception.InsufficientFunds;
import com.betpawa.test.model.Account;
import com.betpawa.test.model.Currency;
import com.betpawa.test.model.User;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * WalletServer gRPC implementation
 */
// constructor based injection for making fields final and thus
// applicable for synchronization
@AllArgsConstructor
@Slf4j
public class WalletServerImpl extends WalletServerGrpc.WalletServerImplBase {

    @Getter
    private final AccountOperator accountOperator;

    @Getter
    private final UserDAO userDAO;

    @Getter
    private final CurrencyDAO currencyDAO;

    @Getter
    private final AccountDAO accountDAO;

    @Override
    public void deposit(All.DepositRequest request, StreamObserver<All.EmptyResponse> responseObserver) {

        try {
            log.debug("check that currency is supported");

            Currency currency;
            //avoid thread starvation due to connection pool acquire request
            synchronized (currencyDAO) {
                currency = currencyDAO.find(request.getMoney().getIsoCode());
            }

            log.debug("getting account for addition to the current balance");
            Account account;
            //avoid thread starvation due to connection pool acquire request
            synchronized (accountDAO) {
                account = accountDAO.find(new User(request.getUserId(), ""), currency);
            }

            log.debug("perform operation");
            accountOperator.deposit(account, request.getMoney());

            log.debug("commit operation");
            //avoid thread starvation due to connection pool acquire request
            synchronized (accountDAO) {
                if (accountDAO.update(account) != 1) {
                    throw new DataNotUpdated("information wasn't updated");
                }
            }

            // non-blocking api to speed up requests handling
            responseObserver.onNext(All.EmptyResponse.newBuilder().build());
            responseObserver.onCompleted();
        } catch (CurrencyNotFound | AccountNotFound | UserNotFound e) {
            responseObserver.onError(Status.NOT_FOUND
                    .withCause(e)
                    .withDescription(e.getClass().getSimpleName())
                    .asException());
        } catch (DataNotUpdated e) {
            responseObserver.onError(Status.INTERNAL
                    .withCause(e)
                    .withDescription(e.getClass().getSimpleName())
                    .asException());
        } catch (Exception e) {
            log.error("Internal Error", e);
            responseObserver.onError(Status.UNKNOWN
                    .withCause(e)
                    .withDescription(e.getLocalizedMessage())
                    .asException());
        }
    }

    @Override
    public void withdraw(All.WithdrawRequest request, StreamObserver<All.EmptyResponse> responseObserver) {

        try {
            log.debug("check that currency is supported");
            Currency currency;
            //avoid thread starvation due to connection pool acquire request
            synchronized (currencyDAO) {
                currency = currencyDAO.find(request.getMoney().getIsoCode());
            }

            log.debug("getting account for subtraction from the current balance");
            Account account;
            //avoid thread starvation due to connection pool acquire request
            synchronized (accountDAO) {
                account = accountDAO.find(new User(request.getUserId(), ""), currency);
            }

            log.debug("perform operation");
            accountOperator.withdraw(account, request.getMoney());

            log.debug("commit operation");
            //avoid thread starvation due to connection pool acquire request
            synchronized (accountDAO) {
                if (accountDAO.update(account) != 1) {
                    throw new DataNotUpdated("information wasn't updated");
                }
            }

            // non-blocking api to speed up requests handling
            responseObserver.onNext(All.EmptyResponse.newBuilder().build());
            responseObserver.onCompleted();
        } catch (CurrencyNotFound | AccountNotFound | UserNotFound e) {
            responseObserver.onError(Status.NOT_FOUND
                    .withCause(e)
                    .withDescription(e.getClass().getSimpleName())
                    .asException());
        } catch (InsufficientFunds e) {
            responseObserver.onError(Status.INVALID_ARGUMENT
                    .withCause(e)
                    .withDescription("insufficient_funds")
                    .asException());
        } catch (DataNotUpdated e) {
            responseObserver.onError(Status.INTERNAL
                    .withCause(e)
                    .withDescription(e.getClass().getSimpleName())
                    .asException());
        } catch (Exception e) {
            log.error("Internal Error", e);
            responseObserver.onError(Status.UNKNOWN
                    .withCause(e)
                    .withDescription(e.getLocalizedMessage())
                    .asException());
        }
    }

    @Override
    public void balance(All.BalanceRequest request, StreamObserver<All.BalanceResponse> responseObserver) {

        try {

            log.debug("check that user exists");
            User user;
            //avoid thread starvation due to connection pool acquire request
            synchronized (userDAO) {
                user = userDAO.find(request.getUserId());
            }

            log.debug("getting accounts related with user");
            List<Account> accounts;
            //avoid thread starvation due to connection pool acquire request
            synchronized (accountDAO) {
                accounts = accountDAO.find(user);
            }

            All.BalanceResponse.Builder builder = All.BalanceResponse.newBuilder();

            log.debug("form response");
            accounts.forEach((account) -> builder.addMoney(All.Money.newBuilder()
                    .setUnit(account.getUnit())
                    .setFraction(account.getFraction())
                    .setIsoCode(account.getCurrency().getIsoCode())
                    .build()));

            // non-blocking api to speed up requests handling
            responseObserver.onNext(builder.build());
            responseObserver.onCompleted();
        } catch (CurrencyNotFound | AccountNotFound | UserNotFound e) {
            responseObserver.onError(Status.NOT_FOUND
                    .withCause(e)
                    .withDescription(e.getClass().getSimpleName())
                    .asException());
        } catch (Exception e) {
            log.error("Internal Error", e);
            responseObserver.onError(Status.UNKNOWN
                    .withCause(e)
                    .withDescription(e.getLocalizedMessage())
                    .asException());
        }
    }

}
