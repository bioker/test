package com.betpawa.test;

import com.betpawa.test.dao.AccountDAO;
import com.betpawa.test.dao.CurrencyDAO;
import com.betpawa.test.dao.UserDAO;
import com.betpawa.test.dao.jdbc.JDBCAccountDAO;
import com.betpawa.test.dao.jdbc.JDBCCurrencyDAO;
import com.betpawa.test.dao.jdbc.JDBCUserDAO;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.beans.PropertyVetoException;
import java.util.Properties;

@Configuration
@Slf4j
public class SpringConfig {

    // environment variables usage for making it easy
    // to control in docker containers

    @Value("#{environment.TEST_SERVER_PORT ?: 9999}")
    private Integer port;
    @Value("#{environment.TEST_DB_DRIVER ?: 'org.postgresql.Driver'}")
    private String driverClassName;
    @Value("#{environment.TEST_DB_URL ?: 'jdbc:postgresql://localhost/test'}")
    private String url;
    @Value("#{environment.TEST_DB_USER ?: 'postgres'}")
    private String username;
    @Value("#{environment.TEST_DB_PASS ?: ''}")
    private String password;
    @Value("#{environment.TEST_DB_CONN_PROP ?: '/c3p0.properties'}")
    private String connectionProperties;

    @Bean
    public WalletServerApp walletServerApp() throws PropertyVetoException {
        log.info("server configuration [port]: [{}]", port);
        WalletServerApp walletServerApp = new WalletServerApp();
        walletServerApp.setPort(port);
        walletServerApp.setWalletServer(walletServer());
        return walletServerApp;
    }

    @Bean
    public WalletServerImpl walletServer() throws PropertyVetoException {
        // constructor based injection to make fields final and use
        // them with synchronization to avoid thread starvation
        // due to connection pool acquire request
        return new WalletServerImpl(
                accountOperator(),
                userDAO(),
                currencyDAO(),
                accountDAO()
        );
    }

    @Bean
    public AccountOperator accountOperator() {
        return new AccountOperator();
    }

    @Bean
    public AccountDAO accountDAO() throws PropertyVetoException {
        JDBCAccountDAO accountDAO = new JDBCAccountDAO();
        accountDAO.setJdbcTemplate(jdbcTemplate());
        accountDAO.setUserDAO(userDAO());
        accountDAO.setCurrencyDAO(currencyDAO());
        return accountDAO;
    }

    @Bean
    public CurrencyDAO currencyDAO() throws PropertyVetoException {
        JDBCCurrencyDAO currencyDAO = new JDBCCurrencyDAO();
        currencyDAO.setJdbcTemplate(jdbcTemplate());
        return currencyDAO;
    }

    @Bean
    public UserDAO userDAO() throws PropertyVetoException {
        JDBCUserDAO userDAO = new JDBCUserDAO();
        userDAO.setJdbcTemplate(jdbcTemplate());
        return userDAO;
    }

    @Bean
    public JdbcTemplate jdbcTemplate() throws PropertyVetoException {
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        jdbcTemplate.setDataSource(dataSource());
        return jdbcTemplate;
    }

    @Bean
    public DataSource dataSource() throws PropertyVetoException {
        log.info("db connection configuration: [{}][{}][{}][{}]",
                driverClassName, url, username, "******");
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        try {
            Properties properties = new Properties();
            properties.load(this.getClass().getResourceAsStream(connectionProperties));
            dataSource.setProperties(properties);
        } catch (Exception e) {
            log.warn("connection properties cannot be loaded", e);
        }
        dataSource.setDriverClass(driverClassName);
        dataSource.setJdbcUrl(url);
        dataSource.setUser(username);
        dataSource.setPassword(password);
        return dataSource;
    }
}
