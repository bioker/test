package com.betpawa.test;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

/**
 * Encapsulation of Server application logic
 * Helps to manipulate application programmatically
 * (helpful for tests for example)
 */
@Slf4j
public class WalletServerApp {

    /**
     * gRPC server port
     */
    @Setter
    @Getter
    private Integer port;

    /**
     * gRPC service implementation
     */
    @Getter
    @Setter
    private WalletServerImpl walletServer;

    private Server server;

    void init() {
        server = ServerBuilder.forPort(port)
                .addService(walletServer)
                .build();
    }

    void start() throws IOException {
        server.start();
        log.info("server started");
    }

    private void awaitTermination() throws InterruptedException {
        server.awaitTermination();
    }

    void stop() {
        server.shutdown();
        log.info("server stopped");
    }

    public static void main(String[] args) throws IOException, InterruptedException {

        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(SpringConfig.class);

        WalletServerApp walletServerApp = context.getBean(WalletServerApp.class);

        log.info("start server");

        walletServerApp.init();
        walletServerApp.start();

        Runtime.getRuntime().addShutdownHook(new Thread(walletServerApp::stop));

        walletServerApp.awaitTermination();
    }

}