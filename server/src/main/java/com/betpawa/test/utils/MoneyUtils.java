package com.betpawa.test.utils;

import javax.money.Monetary;
import javax.money.MonetaryAmount;

/**
 * Helper class to perform operations with money values
 */
public class MoneyUtils {
    public static MonetaryAmount build(String isoCode, Integer unit, Integer fraction) {
        String number = unit + "." + fraction;
        return Monetary.getDefaultAmountFactory()
                .setCurrency(isoCode)
                .setNumber(Double.valueOf(number))
                .create();
    }

    public static Integer getUnit(MonetaryAmount monetaryAmount) {
        return Math.toIntExact(monetaryAmount.getNumber().longValue());
    }

    public static Integer getFraction(MonetaryAmount monetaryAmount) {
        return Math.toIntExact(monetaryAmount.getNumber().getAmountFractionNumerator());
    }
}
