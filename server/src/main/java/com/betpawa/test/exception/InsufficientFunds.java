package com.betpawa.test.exception;

import com.betpawa.test.All;
import com.betpawa.test.model.Account;
import lombok.Getter;

public class InsufficientFunds extends Exception {

    @Getter
    private Account account;
    @Getter
    private All.Money requestedMoney;

    public InsufficientFunds(Account account, All.Money requestedMoney) {
        super();
        this.account = account;
        this.requestedMoney = requestedMoney;
    }

}
