package com.betpawa.test.exception;

/**
 * Exception is used in case of query have not affected any rows
 * and this behavior is unexpected
 */
public class DataNotUpdated extends Exception {

    public DataNotUpdated(String message) {
        super(message);
    }
}
