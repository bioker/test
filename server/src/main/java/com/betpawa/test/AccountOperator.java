package com.betpawa.test;

import com.betpawa.test.exception.InsufficientFunds;
import com.betpawa.test.model.Account;
import com.betpawa.test.utils.MoneyUtils;
import lombok.extern.slf4j.Slf4j;

import javax.money.MonetaryAmount;

/**
 * Class for encapsulation of Account operations
 */
@Slf4j
public class AccountOperator {

    public void deposit(Account account, All.Money money) {
        log.debug("perform deposit operation on account object");
        MonetaryAmount accountAmount = fromAccount(account);
        MonetaryAmount requestAmount = fromRequest(money);

        MonetaryAmount result = accountAmount.add(requestAmount);
        account.setUnit(MoneyUtils.getUnit(result));
        account.setFraction(MoneyUtils.getFraction(result));
    }

    public void withdraw(Account account, All.Money money) throws InsufficientFunds {
        log.debug("perform withdraw operation on account object");
        MonetaryAmount accountAmount = fromAccount(account);
        MonetaryAmount requestAmount = fromRequest(money);

        if (accountAmount.isNegative() || accountAmount.isZero() ||
                accountAmount.isLessThan(requestAmount)) {
            throw new InsufficientFunds(account, money);
        }

        MonetaryAmount result = accountAmount.subtract(requestAmount);
        account.setUnit(MoneyUtils.getUnit(result));
        account.setFraction(MoneyUtils.getFraction(result));
    }

    private MonetaryAmount fromAccount(Account account) {
        return MoneyUtils.build(
                account.getCurrency().getIsoCode(),
                account.getUnit(),
                account.getFraction());
    }

    private MonetaryAmount fromRequest(All.Money money) {
        return MoneyUtils.build(
                money.getIsoCode(),
                money.getUnit(),
                money.getFraction());
    }

}
