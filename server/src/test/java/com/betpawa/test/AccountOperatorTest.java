package com.betpawa.test;

import com.betpawa.test.exception.InsufficientFunds;
import com.betpawa.test.model.Account;
import com.betpawa.test.model.Currency;
import com.betpawa.test.model.User;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class AccountOperatorTest {

    @Test
    public void deposit() {

        Account account = new Account();
        account.setUnit(0);
        account.setFraction(0);
        account.setCurrency(new Currency(1, "USD"));
        account.setUser(new User(1, "User"));

        All.Money money = All.Money.newBuilder()
                .setUnit(1)
                .setFraction(123)
                .setIsoCode("USD")
                .build();

        AccountOperator accountOperator = new AccountOperator();
        accountOperator.deposit(account, money);

        Assert.assertEquals("unit part doesn't match with request value",
                1, account.getUnit().intValue());
        Assert.assertEquals("fraction part doesn't match with request value",
                123, account.getFraction().intValue());
    }

    @Test(expected = InsufficientFunds.class)
    public void withdrawFromZero() throws InsufficientFunds {

        Account account = new Account();
        account.setUnit(0);
        account.setFraction(0);
        account.setCurrency(new Currency(1, "USD"));
        account.setUser(new User(1, "User"));

        All.Money money = All.Money.newBuilder()
                .setUnit(1)
                .setFraction(123)
                .setIsoCode("USD")
                .build();

        AccountOperator accountOperator = new AccountOperator();
        accountOperator.withdraw(account, money);
    }

    @Test(expected = InsufficientFunds.class)
    public void withdrawGreater() throws InsufficientFunds {

        Account account = new Account();
        account.setUnit(1);
        account.setFraction(1);
        account.setCurrency(new Currency(1, "USD"));
        account.setUser(new User(1, "User"));

        All.Money money = All.Money.newBuilder()
                .setUnit(1)
                .setFraction(123)
                .setIsoCode("USD")
                .build();

        AccountOperator accountOperator = new AccountOperator();
        accountOperator.withdraw(account, money);
    }

    @Test
    @Ignore(value = "Make a complete implementation of the processing for a fractional part")
    public void withdraw() throws InsufficientFunds {

        Account account = new Account();
        account.setUnit(1);
        account.setFraction(133);
        account.setCurrency(new Currency(1, "USD"));
        account.setUser(new User(1, "User"));

        All.Money money = All.Money.newBuilder()
                .setUnit(1)
                .setFraction(123)
                .setIsoCode("USD")
                .build();

        AccountOperator accountOperator = new AccountOperator();
        accountOperator.withdraw(account, money);

        Assert.assertEquals(0, account.getUnit().intValue());
        Assert.assertEquals(10, account.getFraction().intValue());
    }
}
