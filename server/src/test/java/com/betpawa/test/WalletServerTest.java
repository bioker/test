package com.betpawa.test;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class WalletServerTest {

    private static WalletServerApp walletServerApp;

    @BeforeClass
    public static void init() {
        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("/applicationContext.xml");

        walletServerApp = context.getBean(WalletServerApp.class);
    }

    /**
     * Integration test
     * 1. Make a withdrawal of USD 200 for user with id 1. Must return "insufficient_funds".
     * 1. Make a deposit of USD 100 to user with id 1.
     * 1. Check that all balances are correct
     * 1. Make a withdrawal of USD 200 for user with id 1. Must return "insufficient_funds".
     * 1. Make a deposit of EUR 100 to user with id 1.
     * 1. Check that all balances are correct
     * 1. Make a withdrawal of USD 200 for user with id 1. Must return "insufficient_funds".
     * 1. Make a deposit of USD 100 to user with id 1.
     * 1. Check that all balances are correct
     * 1. Make a withdrawal of USD 200 for user with id 1. Must return "ok".
     * 1. Check that all balances are correct
     * 1. Make a withdrawal of USD 200 for user with id 1. Must return "insufficient_funds".
     */
    @Test
    public void test() throws IOException {

        walletServerApp.init();
        walletServerApp.start();

        ManagedChannel channel = ManagedChannelBuilder.forAddress(
                "localhost", walletServerApp.getPort()).usePlaintext().build();

        WalletServerGrpc.WalletServerBlockingStub blockingStub =
                WalletServerGrpc.newBlockingStub(channel);

        withdraw(blockingStub, 200, true);
        deposit(blockingStub, 100, "USD");
        checkBalances(blockingStub, getExpectedBalances(100, 0, 0));
        withdraw(blockingStub, 200, true);
        deposit(blockingStub, 100, "EUR");
        checkBalances(blockingStub, getExpectedBalances(100, 100, 0));
        withdraw(blockingStub, 200, true);
        deposit(blockingStub, 100, "USD");
        checkBalances(blockingStub, getExpectedBalances(200, 100, 0));
        withdraw(blockingStub, 200, false);
        checkBalances(blockingStub, getExpectedBalances(0, 100, 0));
        withdraw(blockingStub, 200, true);

    }

    private Map<String, Integer> getExpectedBalances(Integer usd, Integer eur, Integer gbp) {
        Map<String, Integer> checkValues = new HashMap<>();
        checkValues.put("USD", usd);
        checkValues.put("EUR", eur);
        checkValues.put("GBP", gbp);
        return checkValues;
    }


    private void checkBalances(WalletServerGrpc.WalletServerBlockingStub blockingStub, Map<String, Integer> expected) {
        All.BalanceResponse balanceResponse = blockingStub.balance(All.BalanceRequest.newBuilder()
                .setUserId(1)
                .build());

        Assert.assertEquals(3, balanceResponse.getMoneyList().size());

        balanceResponse.getMoneyList().forEach(
                money -> {
                    Assert.assertTrue(expected.containsKey(money.getIsoCode()));
                    Assert.assertEquals(expected.get(money.getIsoCode()).intValue(),
                            money.getUnit());
                }
        );
    }

    private void deposit(WalletServerGrpc.WalletServerBlockingStub blockingStub, Integer amount, String isoCode) {
        blockingStub.deposit(All.DepositRequest.newBuilder()
                .setUserId(1)
                .setMoney(All.Money.newBuilder()
                        .setUnit(amount)
                        .setIsoCode(isoCode)
                        .build())
                .build());
    }


    private void withdraw(WalletServerGrpc.WalletServerBlockingStub blockingStub, Integer amount,
                          boolean insufficient_founds_expected) {
        try {
            blockingStub.withdraw(All.WithdrawRequest.newBuilder()
                    .setUserId(1)
                    .setMoney(All.Money.newBuilder()
                            .setUnit(amount)
                            .setIsoCode("USD")
                            .build())
                    .build());
            if (insufficient_founds_expected) {
                throw new RuntimeException("error must occurs");
            }
        } catch (StatusRuntimeException e) {
            if (insufficient_founds_expected) {
                Assert.assertEquals(Status.INVALID_ARGUMENT.getCode(),
                        e.getStatus().getCode());
                Assert.assertEquals("insufficient_funds",
                        e.getStatus().getDescription()
                );
            } else {
                throw e;
            }
        }
    }

    @AfterClass
    public static void shutdown() {
        if (walletServerApp != null) {
            walletServerApp.stop();
        }
    }

}
