package com.betpawa.test.utils;

import org.junit.Assert;
import org.junit.Test;

import javax.money.Monetary;
import javax.money.MonetaryAmount;

public class MoneyUtilsTest {

    @Test
    public void build() {
        Assert.assertEquals("values don't match with initial ones",
                1231.1235,
                MoneyUtils.build("USD", 1231, 1235)
                        .getNumber().doubleValue(), 0);
    }

    @Test
    public void getUnit() {

        MonetaryAmount oneAndHalf = Monetary.getDefaultAmountFactory()
                .setCurrency("USD")
                .setNumber(112312.235252)
                .create();

        Assert.assertEquals("unit part doesn't match with initial one",
                112312, MoneyUtils.getUnit(oneAndHalf).intValue());
    }

    @Test
    public void getFraction() {

        MonetaryAmount testValue = Monetary.getDefaultAmountFactory()
                .setCurrency("USD")
                .setNumber(112312.235252)
                .create();

        Assert.assertEquals("fraction part doesn't match with initial one",
                235252, MoneyUtils.getFraction(testValue).intValue());
    }
}
